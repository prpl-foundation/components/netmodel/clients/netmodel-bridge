# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.0.4 - 2024-05-06(07:08:33 +0000)

### Other

- Data model parameters needed for IGMPs in Routing Page

## Release v1.0.3 - 2022-10-18(14:24:25 +0000)

### Fixes

- The Management bridge port must not have the "bridgeport" tag set, only the "bridge" tag.

## Release v1.0.2 - 2022-03-03(16:08:08 +0000)

### Fixes

- [NetModel-bridge] The netdev flag should not be cleared if the Port is not a VLAN

## Release v1.0.1 - 2022-02-28(14:50:52 +0000)

### Other

- [netmodel][tr181-bridge] Bridge Management ports must have the netdev flag set

## Release v1.0.0 - 2022-02-28(10:49:53 +0000)

### Breaking

- Use amxm to start/stop syncing netmodel clients

## Release v0.1.0 - 2022-02-03(14:03:08 +0000)

### New

- Integrate support for bridging in NetModel

